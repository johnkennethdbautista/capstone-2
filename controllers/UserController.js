const User = require("../models/User")
const auth = require('../auth')
const bcrypt = require("bcrypt")

// User Register
module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 5)
	})

	return new_user.save().then((registered_user, error) => {
		if(error){
			return error
		}

		return {
			message: 'You have been successfully registered!',
			data: new_user
		}
	})
}

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then(result => {
		if(result.length > 0){
			return true 
		} 

		return false
	})
}

// User Login
module.exports.loginUser = (request_body) => {
	return User.findOne({email:request_body.email}).then(result => {
		if(result === null){
			return {
				message: `${request_body.email} doesn't exist!`
			}
		}

		const isPasswordCorrect = bcrypt.compareSync(request_body.password, result.password)

		if(isPasswordCorrect){
			return {
				accessToken: auth.createAccessToken(result.toObject())
			}
		}

		return {
			message: 'The email and password combination is not correct!'
		}
	})
}

// User Profile
module.exports.userProfile = (userId) => {
	return User.findById(userId).then(result => {
		return result
	})
}