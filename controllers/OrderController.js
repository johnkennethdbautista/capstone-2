const Order = require("../models/Order")
const Product = require("../models/Product")
const User = require('../models/User')
const auth = require('../auth')

// Add Order
module.exports.addOrder = async (user_id, request_body) => {
	let check_existing_user_order = await Order.findOne({ userId: user_id })
	let product = await Product.findById(request_body.productId)
	let order_active = await product.isActive
	let product_price = product.price
	if (check_existing_user_order !== null && order_active) {
		check_existing_user_order.products.push({
			productId: request_body.productId,
			quantity: request_body.quantity,
			totalAmount: product_price * request_body.quantity
		})
		const updated_order = await check_existing_user_order.save()

		return {
			message: 'Order updated successfully!',
			data: updated_order
		}
	} else if (!order_active){
		return {
			message: 'The product is currently unavailable',
			data: product
		}
	} else {
		let new_order = new Order({
			userId: user_id,
			products: [
			{
				productId: request_body.productId,
				quantity: request_body.quantity,
				totalAmount: product_price * request_body.quantity
			}
			]
		})
		const created_order = await new_order.save()

		return {
			message: 'Order added successfully!',
			data: created_order
		}
	}

}

// Check Price
module.exports.checkPrice = (user_id) => {
	return User.findById(user_id).then(async result =>{
		let check_order = await Order.findOne({ userId: user_id })
		if (!check_order) {
			return 'You do not have any order yet!'
		}
		let pending_payment = check_order.products.filter(product => !product.isPaid)
		const totalPrice = pending_payment.reduce((acc, product) => {
			return acc + product.totalAmount;
		}, 0)
		if (totalPrice == 0){
			return 'You do not have pending order yet!'
		}
		return {
			message: 'The total price for your orders is:',
			data: totalPrice
		}
	})
}

// Payment Transaction
module.exports.payment = async (user_id, request_body) => {
	let check_order = await Order.findOne({ userId: user_id })

	if (!check_order) {
		return {
			message: 'You do not have any order yet!',
			icon: 'error',
			title: 'Payment Denied'
		}
	}
	let pending_payment = check_order.products.filter(product => !product.isPaid)
	const totalPrice = pending_payment.reduce((acc, product) => {
		return acc + product.totalAmount;
	}, 0)
	const payment = totalPrice - request_body.payment
	if((payment == 0 || payment < 0) && totalPrice > 0){
		pending_payment.forEach(product => {
			product.isPaid = true;
		});
		await check_order.save()
		return {
			icon: "success",
			title: "Payment Success",
			message: `Your total change is ${Math.abs(payment)}`,
			totalPrice: totalPrice
		}
	} else if(payment > 0){
		return {
			icon: "error",
			title: "Payment Insufficient",
			message: `Your payment is insufficient, you needed to pay additional of PHP ${payment}`
		}
	} else if(totalPrice == 0){
		return {
			icon: "error",
			title: "No Order Found",
			message: 'You do not have any pending order yet!'
		}
	}

}

// Get All Orders
module.exports.getAllOrder = (user_id) => {
	return User.findById(user_id).then(result =>{
		if(result.isAdmin){
			return Order.find({}).then(result => {
				return {
					message: 'Please see the orders below:',
					data: result
				}}
				)
		}

		return 'You are not authorized!'
	})
}

module.exports.getNotPaidSingleOrder = (user_id) => {
	return User.findById(user_id).then(result =>{
			return Order.find({userId: user_id}).then(result => {
					const unpaidOrders = result[0].products.filter((product) => !product.isPaid);

					return{
						message: "Here's the unpaid orders:",
						data: unpaidOrders
					}
				}
				)
	})
}

module.exports.getPaidSingleOrder = (user_id) => {
	return User.findById(user_id).then(result =>{
			return Order.find({userId: user_id}).then(result => {
					const unpaidOrders = result[0].products.filter((product) => product.isPaid);

					return{
						message: "Here's the unpaid orders:",
						data: unpaidOrders
					}
				}
				)
	})
}

module.exports.removeSingleProduct = (user_id, order_id) => {
	return User.findById(user_id)
		.then(user => {
			return Order.findOne({ userId: user_id })
				.then(order => {
					order.products = order.products.filter(product => product.id !== order_id);
					return order.save();
				})
				.then(updatedOrder => {
					return {
						message: `Here's the updated products`,
						data: updatedOrder
					};
				});
		});
};