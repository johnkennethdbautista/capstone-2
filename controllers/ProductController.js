const Product = require("../models/Product")
const User = require("../models/User")
const auth = require('../auth')

// Add Product
module.exports.addProduct = (user_id, request_body) => {
	return User.findById(user_id).then(result =>{
		if(result.isAdmin){
			let newProduct = new Product({
				name: request_body.name,
				description: request_body.description,
				price: request_body.price,
				isActive: request_body.isActive,
				createdOn: request_body.createdOn
			})

			return newProduct.save().then((added_product, error) => {
				if(error){
					return error
				}

				return {
					message: 'Product has been added!',
					data: newProduct
				}
			})
		}

		return 'You are not authorized!'
	})
}

// Get All Products
module.exports.displayProduct = (request_body) => {
	return Product.find({}).then(result => {
		return result
	})
}

// Get Active Products
module.exports.activeProduct = (request_body) => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// Get Single Product
module.exports.singleProduct = (product_id) => {
	return Product.findById(product_id).then(result => {
		return result
	})
}

module.exports.singleProductName = (product_id) => {
	return Product.findById(product_id).then(result => {
		return {
			name: result.name
		}
	})
}

// Update Product Information
module.exports.updateProduct = (product_id, user_id,request_body) => {
	return User.findById(user_id).then(result =>{
		if(result.isAdmin){
			let updatedProduct = {
				name: request_body.name,
				description: request_body.description,
				price: request_body.price,
				isActive: request_body.isActive,
				createdOn: request_body.createdOn
			}

			return Product.findByIdAndUpdate(product_id, updatedProduct).then((modified_product, error) => {
				if(error){
					return {
						message: "error",
						data: error
					}
				}

				return {
					message: 'Product has been updated!',
					data: modified_product
				}
			})
		}
		
		return 'You are not authorized!'
	})
}


// Archive Product
module.exports.archiveProduct = (product_id, request_body) => {
	return User.findById(request_body).then(result =>{
		if(result.isAdmin){
			let updatedProduct = {
				isActive: false
			}

			return Product.findByIdAndUpdate(product_id, updatedProduct).then((modified_product, error) => {
				if(error){
					return error
				}

				return {
					message: 'Product has been put into arhive!',
					data: modified_product
				}
			})
		}
		
		return 'You are not authorized!'
	})
}

// Activate Product
module.exports.activateProduct = (product_id, request_body) => {
	return User.findById(request_body).then(result =>{
		if(result.isAdmin){
			let updatedProduct = {
				isActive: true
			}

			return Product.findByIdAndUpdate(product_id, updatedProduct).then((modified_product, error) => {
				if(error){
					return error
				}

				return {
					message: 'Product has been now activated!',
					data: modified_product
				}
			})
		}
		
		return 'You are not authorized!'
	})
}