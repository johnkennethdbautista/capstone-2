// Server Dependencies
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
require('dotenv').config()
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.ucfa5iz.mongodb.net/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log('Connected to MongoDB!'))

// Server Setup
const app = express()

// Middleware
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Routes
app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)

// Server Listening
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now running on port ${process.env.PORT || 4000}`)
})