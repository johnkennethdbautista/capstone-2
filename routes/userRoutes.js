const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/UserController')

// Router for User Profile
router.get('/profile', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)

	UserController.userProfile(userProfile.id).then(result => response.send(result))
})

// Router for User Register
router.post('/register', (request,response) => {
	UserController.registerUser(request.body).then(result => response.send(result))
})

// Router for User Login
router.post('/login', (request, response) => {
	UserController.loginUser(request.body).then(result => response.send(result))
})

router.post("/check-email", (request, response) => {
	UserController.checkEmailExists(request.body).then(result => response.send(result))
})

module.exports = router