const express = require('express')
const router = express.Router()
const auth = require('../auth')
const OrderController = require('../controllers/OrderController')

// Add Product
router.post('/add-product', auth.verify, (request, response) => {

	const userProfile = auth.decode(request.headers.authorization)

	OrderController.addOrder(userProfile.id, request.body).then(result => response.send(result))

})
// Check Total Price
router.get('/check-price', auth.verify, (request, response) => {

	const userProfile = auth.decode(request.headers.authorization)

	OrderController.checkPrice(userProfile.id).then(result => response.send(result))

})

// Payment Transaction
router.post('/payment', auth.verify, (request, response) => {

	const userProfile = auth.decode(request.headers.authorization)

	OrderController.payment(userProfile.id,request.body).then(result => response.send(result))

})

// Get All Order
router.get('/', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)
	OrderController.getAllOrder(userProfile.id).then(result => response.send(result))
})

router.get('/my-order-pending', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)
	OrderController.getNotPaidSingleOrder(userProfile.id).then(result => response.send(result))
})

router.get('/my-order-paid', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)
	OrderController.getPaidSingleOrder(userProfile.id).then(result => response.send(result))
})

router.delete('/:id/delete-order', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)
	OrderController.removeSingleProduct(userProfile.id, request.params.id).then(result => response.send(result))
})

module.exports = router