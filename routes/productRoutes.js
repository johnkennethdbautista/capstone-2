const express = require('express')
const router = express.Router()
const auth = require('../auth')
const ProductController = require('../controllers/ProductController')

// Add Product
router.post('/add-product', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)

	ProductController.addProduct(userProfile.id, request.body).then(result => response.send(result))
})

// Get All Product
router.get('/', (request, response) => {
	ProductController.displayProduct().then(result => response.send(result))
})

// Get All Product
router.get('/active', (request, response) => {
	ProductController.activeProduct().then(result => response.send(result))
})

// Get All Product
router.get('/:id/single', (request, response) => {
	ProductController.singleProduct(request.params.id).then(result => response.send(result))
})

router.get('/:id/single-name', (request, response) => {
	ProductController.singleProductName(request.params.id).then(result => response.send(result))
})

// Update Product Information
router.patch('/:id/update', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)

	ProductController.updateProduct(request.params.id, userProfile.id, request.body).then(result => response.send(result))
})

// Archive Product
router.patch('/:id/archive', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)

	ProductController.archiveProduct(request.params.id,userProfile.id).then(result => response.send(result))
})

// Activate Product
router.patch('/:id/activate', auth.verify, (request, response) => {
	const userProfile = auth.decode(request.headers.authorization)

	ProductController.activateProduct(request.params.id,userProfile.id).then(result => response.send(result))
})
module.exports = router